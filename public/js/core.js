//   /\_|_/\   M0THPL|>Y × SAMOKAT
//  /       \  dice [autumn 2022]
//÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×÷×
const mainRenderer = new THREE.WebGLRenderer({ alpha: true, antialias: true, powerPreference: "high-performance" });
mainRenderer.setSize(window.innerWidth, window.innerHeight);
mainRenderer.setPixelRatio(window.devicePixelRatio);
document.body.appendChild(mainRenderer.domElement);
mainRenderer.domElement.style.position = 'fixed';
mainRenderer.domElement.style.display = 'block';
mainRenderer.domElement.style.top = 0;
const mainScene = new THREE.Scene();
const mainCamera = new THREE.OrthographicCamera(window.innerWidth / -2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / -2, 1, 360);
const mainContainer = new THREE.Object3D();
mainScene.add(mainContainer);
window.isMobile = function() {
  var check = false;
  (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
}
let loadingCount = 26;
let path = [];
let pic = [];
for (let i = 0; i < 3; i++) {
  loadSVG(`svg/svg${i}.svg`).then(data => {
    path[i] = data.paths;
    loadingCount ? loadingCount-- : createGraphics();
  });
}
function loadSVG(url) {
  return new Promise(resolve => {
    new THREE.SVGLoader().load(url, resolve)
  })
}
for (let i = 0; i < 21; i++) {
  loadPIC(`textures/pic${i}.png`).then(texture => {
    pic[i] = new THREE.MeshBasicMaterial({ map: texture, transparent: true, opacity: 0 });
    loadingCount ? loadingCount-- : createGraphics();
  });
}
function loadPIC(url) {
  return new Promise(resolve => {
    new THREE.TextureLoader().load(url, resolve)
  })
}
let EuclidCircularAMedium;
let EuclidCircularABold;
let EuclidCircularARegular;
new THREE.FontLoader().load('fonts/EuclidCircularAMedium.json', function(font) {
  EuclidCircularAMedium = font;
  loadingCount ? loadingCount-- : createGraphics();
});
new THREE.FontLoader().load('fonts/EuclidCircularABold.json', function(font) {
  EuclidCircularABold = font;
  loadingCount ? loadingCount-- : createGraphics();
});
new THREE.FontLoader().load('fonts/EuclidCircularARegular.json', function(font) {
  EuclidCircularARegular = font;
  loadingCount ? loadingCount-- : createGraphics();
});
const blackMaterial = new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 0 });
const diceBlackMaterial = new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 1 });
const whiteMaterial = new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 });
const redColor = new THREE.Color(0xe93a62);
const page = [new THREE.Object3D(), new THREE.Object3D()];
const border = [];
const svgPic = [];
const sandwich = [];
let backPic = new THREE.Object3D();
const appButton = [];
const sparkle = [];
const whiteSparkle = [];
const diceContainer = [new THREE.Object3D(), new THREE.Object3D()];
const eyes = [new THREE.Object3D()];
const stuffName = [new THREE.Object3D(), new THREE.Object3D()];
const introText = [new THREE.Object3D(), new THREE.Object3D()];
const footerText = [new THREE.Object3D(), new THREE.Object3D()];
const legalText = [];
let slideIcon = new THREE.Object3D();
const carpet = [new THREE.Object3D(), new THREE.Object3D()];
let firstRoll = true;
let portraitContainerOffsetY;
let albumContainerOffsetY;
const someText = [
  [ `Посмотри на карту —`,
    `на противоположной стороне тебя ждёт`,
    `компания друзей и сказочное застолье.`,
    `Скорее беги к ним, пока`,
    `всё не остыло!` ],
  [ `Чтобы узнать, на какое число`,
    `клеточек двигаться, кидай кубик`,
    `и готовься к интересным`,
    `знакомствам по пути` ],
  [ `Повтори этот`,
    `пир в реальности!`,
    `Закажи всё, что нужно для застолья,`,
    `в Самокате — и вперёд к приключениям`,
    `на сытый желудок!` ],
  [ `Посмотри на карту — на противоположной стороне тебя`,
    `ждёт компания друзей и сказочное застолье. Скорее беги`,
    `к ним, пока всё не остыло!` ],
  [ `Чтобы узнать, на какое число клеточек`,
    `двигаться, кидай кубик и готовься`,
    `к интересным знакомствам по пути` ],
  [ `Повтори этот пир в реальности!`,
    `Закажи всё, что нужно для застолья, в Самокате —`,
    `и вперёд к приключениям на сытый желудок!` ],
  [ `Продавец ООО «Умный ритейл», ОГРН 1177847261602, 192019, Санкт-Петербург,`,
    `ул.Седова, дом.11, Литер А, Этаж 6, помещение 627. Зоны доставки и точное`,
    `время доставки уточняйте в мобильном приложении «Самокат».` ],
  [ `Продавец ООО «Умный ритейл», ОГРН 1177847261602, 192019, Санкт-Петербург, ул.Седова, дом.11, Литер А, Этаж 6,`,
    `помещение 627. Зоны доставки и точное время доставки уточняйте в мобильном приложении «Самокат».` ]
];
function createGraphics() {
  pic[14].opacity = 1;
  pic[14].alphaTest = 0.5;
  backPic.pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(2160, 2408), pic[14]);
  backPic.fog = new THREE.Mesh(new THREE.PlaneBufferGeometry(2160, 2408), new THREE.MeshBasicMaterial({ color: 0xe8c2b5, transparent: true, opacity: 1 }));
  backPic.fog.position.z = 1;
  backPic.add(backPic.pic, backPic.fog);
  backPic.position.z = -353;
  createSVG(0, 12, 0xe8c2b5, 0);
  svgPic[0].scale.set(1.1, -1.1, 1);
  svgPic[0].position.set(-76, 13, 0);
  svgPic[0].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(150, 30), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  svgPic[0].plane.position.set(67, 10, 0);
  svgPic[0].add(svgPic[0].plane);
  svgPic[0].box.position.set(0, -35, 1);
  svgPic[0].box.scale.set(0.5, 0.5, 1);
  createSVG(1, 12, 0xe8c2b5, 0);
  svgPic[1].scale.set(1.5, -1.5, 1);
  svgPic[1].position.set(-105, 13, 0);
  svgPic[1].box.position.set(0, -110, 1);
  svgPic[1].box.scale.set(0.5, 0.5, 1);
  stuffName[0].line = [];
  stuffName[0].line[0] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`Застольная`, 31), 3), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 0 }));
  stuffName[0].line[0].geometry.computeBoundingBox();
  stuffName[0].line[0].geometry.translate(-0.5 * stuffName[0].line[0].geometry.boundingBox.max.x, 0, 0);
  stuffName[0].line[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(`настолка`, 31), 3), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 0 }));
  stuffName[0].line[1].geometry.computeBoundingBox();
  stuffName[0].line[1].geometry.translate(-0.5 * stuffName[0].line[1].geometry.boundingBox.max.x, 0, 0);
  stuffName[0].line[1].position.y = -42;
  stuffName[0].add(stuffName[0].line[0], stuffName[0].line[1]);
  stuffName[0].position.set(0, -220, 0);
  stuffName[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularABold.generateShapes(`ЗАСТОЛЬНАЯ НАСТОЛКА`, 48), 4), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 0 }));
  stuffName[1].geometry.center();
  stuffName[1].position.y = -300;
  eyes[0].part = [];
  eyes[0].part[0] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[0].map.image.width * 0.26, pic[0].map.image.height * 0.26), pic[0]);
  eyes[0].part[1] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[1].map.image.width * 0.26, pic[1].map.image.height * 0.26), pic[1]);
  eyes[0].part[1].position.z = 0.1;
  eyes[0].add(eyes[0].part[0], eyes[0].part[1]);
  eyes[0].scale.y = 0;
  eyes[0].position.set(0, -285, 0);
  introText[0].line = [];
  for (let i = 0; i < 5; i++) {
    introText[0].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[0][i], 11), 2), new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 0 }));
    introText[0].line[i].geometry.computeBoundingBox();
    introText[0].line[i].geometry.translate(-0.5 * introText[0].line[i].geometry.boundingBox.max.x, 0, 0);
    introText[0].line[i].position.y -= 20 * i;
    introText[0].add(introText[0].line[i]);
  }
  introText[0].line[3].position.y -= 10;
  introText[0].line[4].position.y -= 10;
  introText[0].position.set(0, -300, 0);
  introText[1].line = [];
  for (let i = 0; i < 3; i++) {
    introText[1].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[3][i], 14), 2), new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 0 }));
    introText[1].line[i].geometry.computeBoundingBox();
    introText[1].line[i].geometry.translate(-0.5 * introText[1].line[i].geometry.boundingBox.max.x, 0, 0);
    introText[1].line[i].position.y -= 28 * i;
    introText[1].add(introText[1].line[i]);
  }
  introText[1].position.set(0, -350, 0);
  page[0].illustration = [];
  page[0].illustration[0] = new THREE.Object3D();
  page[0].illustration[0].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[2].map.image.width * 0.25, pic[2].map.image.height * 0.25), pic[2]);
  page[0].illustration[0].pic.position.set(-7, -7, 0);
  gsap.to(page[0].illustration[0].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[0].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[0].add(page[0].illustration[0].pic);
  page[0].illustration[0].position.set(-110, -460, 2);
  page[0].illustration[0].rotation.z = 0.05;
  page[0].illustration[1] = new THREE.Object3D();
  page[0].illustration[1].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[3].map.image.width * 0.24, pic[3].map.image.height * 0.24), pic[3]);
  page[0].illustration[1].pic.position.set(-7, -7, 0);
  gsap.to(page[0].illustration[1].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[1].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[1].add(page[0].illustration[1].pic);
  page[0].illustration[1].position.set(110, -510, 2);
  page[0].illustration[1].rotation.z = 0.05;
  gsap.to(page[0].illustration[1].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[2] = new THREE.Object3D();
  page[0].illustration[2].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[4].map.image.width * 0.21, pic[4].map.image.height * 0.21), pic[4]);
  page[0].illustration[2].pic.position.set(-7, -7, 0);
  gsap.to(page[0].illustration[2].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[2].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[2].add(page[0].illustration[2].pic);
  page[0].illustration[2].position.set(-75, -675, 2);
  page[0].illustration[2].rotation.z = 0.05;
  gsap.to(page[0].illustration[2].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[3] = new THREE.Object3D();
  page[0].illustration[3].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[5].map.image.width * 0.18, pic[5].map.image.height * 0.18), pic[5]);
  page[0].illustration[3].pic.position.set(-3, -7, 0);
  gsap.to(page[0].illustration[3].pic.position, { duration: 2 + Math.random() * 3, x: 3, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[3].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[3].add(page[0].illustration[3].pic);
  page[0].illustration[3].position.set(148, -90, 2);
  page[0].illustration[3].rotation.z = 0.35;
  gsap.to(page[0].illustration[3].rotation, { duration: 2 + Math.random() * 3, z: 0.25, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[4] = new THREE.Object3D();
  page[0].illustration[4].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[7].map.image.width * 0.2, pic[7].map.image.height * 0.2), pic[7]);
  page[0].illustration[4].pic.position.set(-7, -7, 0);
  gsap.to(page[0].illustration[4].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[4].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[4].add(page[0].illustration[4].pic);
  page[0].illustration[4].position.set(-160, -1550, 3);
  page[0].illustration[4].rotation.z = -1.1;
  gsap.to(page[0].illustration[4].rotation, { duration: 2 + Math.random() * 3, z: -1.2, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[5] = new THREE.Object3D();
  page[0].illustration[5].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[7].map.image.width * 0.21, pic[7].map.image.height * 0.21), pic[7]);
  page[0].illustration[5].pic.position.set(-7, -7, 0);
  gsap.to(page[0].illustration[5].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[0].illustration[5].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[0].illustration[5].add(page[0].illustration[5].pic);
  page[0].illustration[5].position.set(160, -740, 2);
  page[0].illustration[5].rotation.z = -1.1;
  gsap.to(page[0].illustration[5].rotation, { duration: 2 + Math.random() * 3, z: -1, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration = [];
  page[1].illustration[0] = new THREE.Object3D();
  page[1].illustration[0].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[2].map.image.width * 0.33, pic[2].map.image.height * 0.33), pic[2]);
  page[1].illustration[0].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[0].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[0].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[0].add(page[1].illustration[0].pic);
  page[1].illustration[0].position.set(-490, -620, 2);
  page[1].illustration[0].rotation.z = 0.05;
  page[1].illustration[1] = new THREE.Object3D();
  page[1].illustration[1].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[3].map.image.width * 0.33, pic[3].map.image.height * 0.33), pic[3]);
  page[1].illustration[1].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[1].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[1].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[1].add(page[1].illustration[1].pic);
  page[1].illustration[1].position.set(480, -980, 2);
  page[1].illustration[1].rotation.z = 0.05;
  gsap.to(page[1].illustration[1].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[2] = new THREE.Object3D();
  page[1].illustration[2].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[4].map.image.width * 0.33, pic[4].map.image.height * 0.33), pic[4]);
  page[1].illustration[2].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[2].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[2].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[2].add(page[1].illustration[2].pic);
  page[1].illustration[2].position.set(540, -590, 2);
  page[1].illustration[2].rotation.z = 0.05;
  gsap.to(page[1].illustration[2].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[3] = new THREE.Object3D();
  page[1].illustration[3].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[5].map.image.width * 0.33, pic[5].map.image.height * 0.33), pic[5]);
  page[1].illustration[3].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[3].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[3].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[3].add(page[1].illustration[3].pic);
  page[1].illustration[3].position.set(370, -740, 2);
  page[1].illustration[3].rotation.z = 0.05;
  gsap.to(page[1].illustration[3].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[4] = new THREE.Object3D();
  page[1].illustration[4].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[6].map.image.width * 0.33, pic[6].map.image.height * 0.33), pic[6]);
  page[1].illustration[4].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[4].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[4].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[4].add(page[1].illustration[4].pic);
  page[1].illustration[4].position.set(-370, -1000, 3);
  page[1].illustration[4].rotation.z = 0.05;
  gsap.to(page[1].illustration[4].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[5] = new THREE.Object3D();
  page[1].illustration[5].pic = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[7].map.image.width * 0.33, pic[7].map.image.height * 0.33), pic[7]);
  page[1].illustration[5].pic.position.set(-7, -7, 0);
  gsap.to(page[1].illustration[5].pic.position, { duration: 2 + Math.random() * 3, x: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(page[1].illustration[5].pic.position, { duration: 2 + Math.random() * 3, y: 7, ease: "power1.inOut", repeat: -1, yoyo: true });
  page[1].illustration[5].add(page[1].illustration[5].pic);
  page[1].illustration[5].position.set(-630, -920, 1);
  page[1].illustration[5].rotation.z = 0.05;
  gsap.to(page[1].illustration[5].rotation, { duration: 2 + Math.random() * 3, z: -0.05, ease: "power1.inOut", repeat: -1, yoyo: true });
  let shape = new THREE.Shape();
  shape.moveTo(-160, -42);
  drawCorner(shape, -160, 0, 42, 33, 26, 0);
  shape.lineTo(118, 0);
  drawCorner(shape, 160, 0, 42, 33, 26, 1);
  shape.lineTo(160, -408);
  drawCorner(shape, 160, -450, 42, 33, 26, 2);
  shape.lineTo(-118, -450);
  drawCorner(shape, -160, -450, 42, 33, 26, 3);
  diceContainer[0].back = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 18), blackMaterial);
  diceContainer[0].dice = new THREE.Object3D();
  diceContainer[0].dice.part = [];
  diceContainer[0].dice.part[0] = new THREE.Mesh(new THREE.BoxBufferGeometry(159, 159, 159), whiteMaterial);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(-287, -42);
  drawCorner(shape, -287, 0, 42, 33, 26, 0);
  shape.lineTo(245, 0);
  drawCorner(shape, 287, 0, 42, 33, 26, 1);
  shape.lineTo(287, -400);
  drawCorner(shape, 287, -442, 42, 33, 26, 2);
  shape.lineTo(-245, -442);
  drawCorner(shape, -287, -442, 42, 33, 26, 3);
  diceContainer[1].back = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 18), blackMaterial);
  diceContainer[1].dice = new THREE.Object3D();
  diceContainer[1].dice.part = [];
  diceContainer[1].dice.part[0] = new THREE.Mesh(new THREE.BoxBufferGeometry(159, 159, 159), whiteMaterial);
  shape = null;
  shape = new THREE.Shape();
  shape.moveTo(80, 80);
  shape.absarc(61, 61, 15, 0, Math.PI / 2);
  shape.absarc(-61, 61, 15, Math.PI / 2, Math.PI);
  shape.absarc(-61, -61, 15, Math.PI, Math.PI * 1.5);
  shape.absarc(61, -61, 15, Math.PI * 1.5, Math.PI * 2);
  shape.lineTo(76, 61);
  shape.lineTo(80, 80);
  shape.lineTo(80, -80);
  shape.lineTo(-80, -80);
  shape.lineTo(-80, 80);
  shape.lineTo(80, 80);
  for (let i = 1; i < 7; i++) {
    diceContainer[0].dice.part[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 10), diceBlackMaterial);
    diceContainer[1].dice.part[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 10), diceBlackMaterial);
  }
  diceContainer[0].dice.part[1].position.z = 80;
  diceContainer[0].dice.part[2].position.z = -80;
  diceContainer[0].dice.part[2].rotation.x = Math.PI;
  diceContainer[0].dice.part[3].position.x = -80;
  diceContainer[0].dice.part[3].rotation.y = -Math.PI / 2;
  diceContainer[0].dice.part[4].position.x = 80;
  diceContainer[0].dice.part[4].rotation.y = Math.PI / 2;
  diceContainer[0].dice.part[5].position.y = 80;
  diceContainer[0].dice.part[5].rotation.x = -Math.PI / 2;
  diceContainer[0].dice.part[6].position.y = -80;
  diceContainer[0].dice.part[6].rotation.x = Math.PI / 2;
  diceContainer[1].dice.part[1].position.z = 80;
  diceContainer[1].dice.part[2].position.z = -80;
  diceContainer[1].dice.part[2].rotation.x = Math.PI;
  diceContainer[1].dice.part[3].position.x = -80;
  diceContainer[1].dice.part[3].rotation.y = -Math.PI / 2;
  diceContainer[1].dice.part[4].position.x = 80;
  diceContainer[1].dice.part[4].rotation.y = Math.PI / 2;
  diceContainer[1].dice.part[5].position.y = 80;
  diceContainer[1].dice.part[5].rotation.x = -Math.PI / 2;
  diceContainer[1].dice.part[6].position.y = -80;
  diceContainer[1].dice.part[6].rotation.x = Math.PI / 2;
  pic[8].opacity = 1;
  pic[9].opacity = 1;
  pic[8].alphaTest = 0.5;
  pic[9].alphaTest = 0.5;
  for (let i = 7; i < 28; i++) {
    const randomPic = 8 + Math.round(Math.random());
    diceContainer[0].dice.part[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[randomPic].map.image.width * 0.33, pic[randomPic].map.image.height * 0.33), pic[randomPic]);
    diceContainer[0].dice.part[i].rotation.z = Math.random() * 6;
    diceContainer[1].dice.part[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[randomPic].map.image.width * 0.33, pic[randomPic].map.image.height * 0.33), pic[randomPic]);
    diceContainer[1].dice.part[i].rotation.z = Math.random() * 6;
  }
  diceContainer[0].dice.part[7].position.z = 80;
  diceContainer[1].dice.part[7].position.z = 80;
  for (let i = 8; i < 14; i++) {
    diceContainer[0].dice.part[i].rotation.x = Math.PI;
    diceContainer[0].dice.part[i].position.set(-43 + (i % 2) * 86, 43 - 43 * Math.floor((i - 8) / 2), -80);
    diceContainer[1].dice.part[i].rotation.x = Math.PI;
    diceContainer[1].dice.part[i].position.set(-43 + (i % 2) * 86, 43 - 43 * Math.floor((i - 8) / 2), -80);
  }
  diceContainer[0].dice.part[14].rotation.x = -Math.PI / 2;
  diceContainer[0].dice.part[14].position.set(-43, 80, -43);
  diceContainer[0].dice.part[15].rotation.x = -Math.PI / 2;
  diceContainer[0].dice.part[15].position.set(43, 80, 43);
  diceContainer[1].dice.part[14].rotation.x = -Math.PI / 2;
  diceContainer[1].dice.part[14].position.set(-43, 80, -43);
  diceContainer[1].dice.part[15].rotation.x = -Math.PI / 2;
  diceContainer[1].dice.part[15].position.set(43, 80, 43);
  for (let i = 16; i < 21; i++) {
    diceContainer[0].dice.part[i].rotation.x = Math.PI / 2;
    diceContainer[1].dice.part[i].rotation.x = Math.PI / 2;
  }
  diceContainer[0].dice.part[16].position.set(-43, -80, -43);
  diceContainer[0].dice.part[17].position.set(-43, -80, 43);
  diceContainer[0].dice.part[18].position.set(43, -80, -43);
  diceContainer[0].dice.part[19].position.set(43, -80, 43);
  diceContainer[0].dice.part[20].position.y = -80;
  diceContainer[1].dice.part[16].position.set(-43, -80, -43);
  diceContainer[1].dice.part[17].position.set(-43, -80, 43);
  diceContainer[1].dice.part[18].position.set(43, -80, -43);
  diceContainer[1].dice.part[19].position.set(43, -80, 43);
  diceContainer[1].dice.part[20].position.y = -80;
  for (let i = 21; i < 24; i++) {
    diceContainer[0].dice.part[i].rotation.y = -Math.PI / 2;
    diceContainer[0].dice.part[i].position.set(-80, -43 + (i - 21) * 43, -43 + (i -21) * 43);
    diceContainer[1].dice.part[i].rotation.y = -Math.PI / 2;
    diceContainer[1].dice.part[i].position.set(-80, -43 + (i - 21) * 43, -43 + (i -21) * 43);
  }
  for (let i = 24; i < 28; i++) {
    diceContainer[0].dice.part[i].rotation.y = Math.PI / 2;
    diceContainer[0].dice.part[i].position.set(80, -43 + (i % 2) * 86, 43 - 86 * Math.floor((i - 24) / 2));
    diceContainer[1].dice.part[i].rotation.y = Math.PI / 2;
    diceContainer[1].dice.part[i].position.set(80, -43 + (i % 2) * 86, 43 - 86 * Math.floor((i - 24) / 2));
  }
  diceContainer[0].text = [];
  for (let i = 0; i < 4; i++) {
    diceContainer[0].text[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[1][i], 11), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
    diceContainer[0].text[i].geometry.computeBoundingBox();
    diceContainer[0].text[i].geometry.translate(-0.5 * diceContainer[0].text[i].geometry.boundingBox.max.x, 0, 0);

    diceContainer[0].text[i].position.set(0, -60 - 22 * i, 290);
    diceContainer[0].add(diceContainer[0].text[i]);
  }
  diceContainer[1].text = [];
  for (let i = 0; i < 3; i++) {
    diceContainer[1].text[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[4][i], 14), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 0 }));
    diceContainer[1].text[i].geometry.computeBoundingBox();
    diceContainer[1].text[i].geometry.translate(-0.5 * diceContainer[1].text[i].geometry.boundingBox.max.x, 0, 0);
    diceContainer[1].text[i].position.set(0, -55 - 26 * i, 290);
    diceContainer[1].add(diceContainer[1].text[i]);
  }
  diceContainer[0].button = new THREE.Object3D();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-110, 0, 25, Math.PI / 2, Math.PI * 1.5);
  shape.absarc(110, 0, 25, Math.PI * 1.5, Math.PI * 2.5);
  diceContainer[0].button.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 24), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 1 }));
  diceContainer[0].button.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularABold.generateShapes(`Кинь меня!`, 14), 4), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  diceContainer[0].button.text.geometry.center();
  diceContainer[0].button.text.position.set(0, 1, 0.2);
  diceContainer[0].button.add(diceContainer[0].button.plane, diceContainer[0].button.text);
  diceContainer[0].button.position.set(0, -395, 290);
  diceContainer[0].button.ready = true;
  diceContainer[1].button = new THREE.Object3D();
  shape = null;
  shape = new THREE.Shape();
  shape.absarc(-175, 0, 25, Math.PI / 2, Math.PI * 1.5);
  shape.absarc(175, 0, 25, Math.PI * 1.5, Math.PI * 2.5);
  diceContainer[1].button.plane = new THREE.Mesh(new THREE.ShapeBufferGeometry(shape, 24), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 1 }));
  diceContainer[1].button.text = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularABold.generateShapes(`Кинь меня!`, 14), 4), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  diceContainer[1].button.text.geometry.center();
  diceContainer[1].button.text.position.set(0, 1, 0.2);
  diceContainer[1].button.add(diceContainer[1].button.plane, diceContainer[1].button.text);
  diceContainer[1].button.position.set(0, -383, 290);
  diceContainer[1].button.ready = true;
  diceContainer[0].dice.tween1 = gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 5 + Math.random() * 8, x: Math.PI * 2, ease: "none", repeat: -1 });
  diceContainer[0].dice.tween2 = gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 5 + Math.random() * 8, y: Math.PI * 2, ease: "none", repeat: -1 });
  diceContainer[0].dice.tween3 = gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 5 + Math.random() * 8, z: -Math.PI * 2, ease: "none", repeat: -1 });
  for (let i = 0; i < diceContainer[0].dice.part.length; i++) {
    diceContainer[0].dice.add(diceContainer[0].dice.part[i]);
    diceContainer[1].dice.add(diceContainer[1].dice.part[i]);
  }
  diceContainer[0].dice.scale.set(0.8, 0.8, 0.8);
  diceContainer[0].dice.position.set(0, -250, 100);
  diceContainer[1].dice.scale.set(0, 0, 0);
  diceContainer[1].dice.position.set(0, -235, 100);
  diceContainer[0].add(diceContainer[0].button, diceContainer[0].back, diceContainer[0].dice);
  diceContainer[0].position.set(0, -780, 2);
  diceContainer[1].add(diceContainer[1].button, diceContainer[1].back, diceContainer[1].dice);
  diceContainer[1].position.set(0, -445, 2);
  sandwich[0] = new THREE.Object3D();
  sandwich[0].container = new THREE.Object3D();
  sandwich[0].part = [];
  pic[10].opacity = 1;
  pic[10].alphaTest = 0.5;
  pic[11].opacity = 1;
  sandwich[0].part[0] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[10].map.image.width * 0.33, pic[10].map.image.height * 0.33), pic[10]);
  sandwich[0].part[1] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[11].map.image.width * 0.33, pic[11].map.image.height * 0.33), pic[11]);
  sandwich[0].part[1].geometry.translate(0.45 * pic[11].map.image.width * 0.33, 0.45 * pic[11].map.image.height * 0.33, 0.1);
  sandwich[0].part[1].position.set(53, 15, 0);
  sandwich[0].part[1].rotation.z = 0.1;
  sandwich[0].part[2] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[11].map.image.width * 0.33, pic[11].map.image.height * 0.33), pic[11]);
  sandwich[0].part[2].geometry.translate(0.45 * pic[11].map.image.width * 0.33, 0.45 * pic[11].map.image.height * 0.33, -0.1);
  sandwich[0].part[2].scale.x = -1;
  sandwich[0].part[2].position.set(-17, 18, 0);
  sandwich[0].part[2].rotation.z = -0.1;
  sandwich[0].container.add(sandwich[0].part[0], sandwich[0].part[1], sandwich[0].part[2]);
  sandwich[0].container.rotation.z = -0.1;
  sandwich[0].add(sandwich[0].container);
  sandwich[0].position.set(110, -1610, 2);
  sandwich[1] = new THREE.Object3D();
  sandwich[1].container = new THREE.Object3D();
  sandwich[1].part = [];
  sandwich[1].part[0] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[10].map.image.width * 0.33, pic[10].map.image.height * 0.33), pic[10]);
  sandwich[1].part[1] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[11].map.image.width * 0.33, pic[11].map.image.height * 0.33), pic[11]);
  sandwich[1].part[1].geometry.translate(0.45 * pic[11].map.image.width * 0.33, 0.45 * pic[11].map.image.height * 0.33, 0.1);
  sandwich[1].part[1].position.set(53, 15, 0);
  sandwich[1].part[1].rotation.z = 0.1;
  sandwich[1].part[2] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[11].map.image.width * 0.33, pic[11].map.image.height * 0.33), pic[11]);
  sandwich[1].part[2].geometry.translate(0.45 * pic[11].map.image.width * 0.33, 0.45 * pic[11].map.image.height * 0.33, -0.1);
  sandwich[1].part[2].scale.x = -1;
  sandwich[1].part[2].position.set(-17, 18, 0);
  sandwich[1].part[2].rotation.z = -0.1;
  sandwich[1].container.add(sandwich[1].part[0], sandwich[1].part[1], sandwich[1].part[2]);
  sandwich[1].container.rotation.z = -0.1;
  sandwich[1].add(sandwich[1].container);
  sandwich[1].scale.set(1.68, 1.68, 1);
  sandwich[1].position.set(-10, -1180, 2);
  gsap.to([sandwich[0].part[1].rotation, sandwich[1].part[1].rotation], { duration: 0.5, z: -0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to([sandwich[0].part[2].rotation, sandwich[1].part[2].rotation], { duration: 0.5, z: 0.2, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to([sandwich[0].container.position, sandwich[1].container.position], { duration: 0.5, y: 4, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to([sandwich[0].container.position, sandwich[1].container.position], { duration: 4, x: -10, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to([sandwich[0].container.rotation, sandwich[1].container.rotation], { duration: 5, z: 0.1, ease: "power1.inOut", repeat: -1, yoyo: true });
  footerText[0].line = [];
  for (let i = 0; i < 2; i++) {
    footerText[0].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[2][i], 23), 2), new THREE.MeshBasicMaterial({ color: 0xe93a62, transparent: true, opacity: 1 }));
    footerText[0].line[i].geometry.computeBoundingBox();
    footerText[0].line[i].geometry.translate(-0.5 * footerText[0].line[i].geometry.boundingBox.max.x, 0, 0);
    footerText[0].line[i].position.y -= 32 * i;
    footerText[0].add(footerText[0].line[i]);
  }
  for (let i = 2; i < 5; i++) {
    footerText[0].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[2][i], 11), 2), new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 1 }));
    footerText[0].line[i].geometry.computeBoundingBox();
    footerText[0].line[i].geometry.translate(-0.5 * footerText[0].line[i].geometry.boundingBox.max.x, 0, 0);
    footerText[0].line[i].position.y -= 30 + 20 * i;
    footerText[0].add(footerText[0].line[i]);
  }
  footerText[0].position.set(0, -1300, 1);
  footerText[1].line = [];
  for (let i = 0; i < 3; i++) {
    footerText[1].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularAMedium.generateShapes(someText[5][i], 14), 2), new THREE.MeshBasicMaterial({ color: 0x222222, transparent: true, opacity: 1 }));
    footerText[1].line[i].geometry.computeBoundingBox();
    footerText[1].line[i].geometry.translate(-0.5 * footerText[1].line[i].geometry.boundingBox.max.x, 0, 0);
    footerText[1].line[i].position.y -= 26 * i;
    footerText[1].add(footerText[1].line[i]);
  }
  footerText[1].position.set(0, -1335, 1);
  pic[12].opacity = 1;
  appButton[0] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[12].map.image.width * 0.24, pic[12].map.image.height * 0.24), pic[12]);
  appButton[1] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[12].map.image.width * 0.24, pic[12].map.image.height * 0.24), pic[12]);
  appButton[0].position.set(0, -1480, 1);
  appButton[1].position.set(0, -1450, 1);
  createSVG(4, 36, 0xe93a62, 2);
  svgPic[4].scale.set(1, -1, 1);
  svgPic[4].box.position.set(-160, -700, -1);
  gsap.to(svgPic[4].position, { duration: 5, y: 40, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(svgPic[4].position, { duration: 4, x: -20, ease: "power1.inOut", repeat: -1, yoyo: true });
  createSVG(2, 36, 0xe93a62, 1);
  svgPic[2].scale.set(1.065, -1.065, 1);
  svgPic[2].box.position.set(-720, -825, -1);
  gsap.to(svgPic[2].position, { duration: 5, y: 40, ease: "power1.inOut", repeat: -1, yoyo: true });
  gsap.to(svgPic[2].position, { duration: 4, x: -70, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[0] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.35, pic[15].map.image.height * 0.35), pic[15]);
  sparkle[1] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.35, pic[15].map.image.height * 0.35), pic[15]);
  sparkle[2] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.35, pic[16].map.image.height * 0.35), pic[16]);
  sparkle[3] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.35, pic[16].map.image.height * 0.35), pic[16]);
  sparkle[4] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.55, pic[15].map.image.height * 0.55), pic[15]);
  sparkle[5] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.55, pic[15].map.image.height * 0.55), pic[15]);
  sparkle[6] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.55, pic[15].map.image.height * 0.55), pic[15]);
  sparkle[7] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.55, pic[15].map.image.height * 0.55), pic[15]);
  sparkle[8] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.55, pic[16].map.image.height * 0.55), pic[16]);
  sparkle[9] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.55, pic[16].map.image.height * 0.55), pic[16]);
  sparkle[10] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.55, pic[16].map.image.height * 0.55), pic[16]);
  sparkle[11] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[16].map.image.width * 0.55, pic[16].map.image.height * 0.55), pic[16]);
  sparkle[12] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[15].map.image.width * 0.35, pic[15].map.image.height * 0.35), pic[15]);
  sparkle[13] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[19].map.image.width * 0.35, pic[19].map.image.height * 0.35), pic[19]);
  sparkle[14] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[19].map.image.width * 0.35, pic[19].map.image.height * 0.35), pic[19]);
  sparkle[15] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[20].map.image.width * 0.35, pic[20].map.image.height * 0.35), pic[20]);
  sparkle[16] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[20].map.image.width * 0.35, pic[20].map.image.height * 0.35), pic[20]);
  sparkle[0].position.set(150, -650, 2);
  sparkle[4].position.set(-650, -810, 2);
  gsap.to([sparkle[0].rotation, sparkle[4].rotation], { duration: 5, z: Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to([sparkle[0].scale, sparkle[4].scale], { duration: 1, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[1].position.set(-150, -1290, 2);
  sparkle[5].position.set(-370, -840, 2);
  gsap.to([sparkle[1].rotation, sparkle[5].rotation], { duration: 4, z: -Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to([sparkle[1].scale, sparkle[5].scale], { duration: 0.8, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[6].position.set(360, -590, 2);
  gsap.to(sparkle[6].rotation, { duration: 4.5, z: Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(sparkle[6].scale, { duration: 0.9, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[7].position.set(630, -760, 2);
  gsap.to(sparkle[7].rotation, { duration: 5.5, z: -Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(sparkle[7].scale, { duration: 0.7, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[2].position.set(35, -430, 2);
  sparkle[8].position.set(-400, -570, 2);
  gsap.to([sparkle[2].scale, sparkle[8].scale], { duration: 0.7, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[3].position.set(-80, -1580, 2);
  sparkle[9].position.set(-190, -1000, 2);
  gsap.to([sparkle[3].scale, sparkle[9].scale], { duration: 0.9, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[10].position.set(410, -830, 2);
  gsap.to(sparkle[10].scale, { duration: 0.8, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[11].position.set(630, -1000, 2);
  gsap.to(sparkle[11].scale, { duration: 0.6, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[12].position.set(100, -1440, 2);
  gsap.to(sparkle[12].rotation, { duration: 6, z: Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(sparkle[12].scale, { duration: 0.9, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[15].position.set(-60, -410, 2);
  gsap.to(sparkle[15].rotation, { duration: 7, z: Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(sparkle[15].scale, { duration: 0.8, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[16].position.set(-105, -1495, 2);
  gsap.to(sparkle[16].rotation, { duration: 7, z: -Math.PI * 2, ease: "none", repeat: -1 });
  gsap.to(sparkle[16].scale, { duration: 0.8, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[13].position.set(-160, -760, 2);
  gsap.to(sparkle[13].scale, { duration: 0.6, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  sparkle[14].position.set(120, -1265, 2);
  gsap.to(sparkle[14].scale, { duration: 0.7, x: 0.6, y: 0.6, ease: "power1.inOut", repeat: -1, yoyo: true });
  pic[17].opacity = 1;
  pic[17].alphaTest = 0.7;
  pic[18].opacity = 1;
  pic[18].alphaTest = 0.7;
  for (let i = 0; i < 12; i++) {
   const randomPic = 17 + Math.round(Math.random());
   whiteSparkle[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[randomPic].map.image.width * 0.35, pic[randomPic].map.image.height * 0.35), pic[randomPic]);
   whiteSparkle[i].scale.set(0, 0, 1);
   diceContainer[0].add(whiteSparkle[i]);
  }
  for (let i = 12; i < 24; i++) {
    const randomPic = 17 + Math.round(Math.random());
    whiteSparkle[i] = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[randomPic].map.image.width * 0.35, pic[randomPic].map.image.height * 0.35), pic[randomPic]);
    whiteSparkle[i].scale.set(0, 0, 1);
    diceContainer[1].add(whiteSparkle[i]);
  }
  legalText[0] = new THREE.Object3D();
  legalText[0].line = [];
  for (let i = 0; i < 3; i++) {
    legalText[0].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(someText[6][i], 6), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
    legalText[0].line[i].position.set(-160, -1725 - 11 * i, 0);
    page[0].add(legalText[0].line[i]);
  }
  createSVG(3, 12, 0xffffff, 0);
  svgPic[3].scale.set(1.1, -1.1, 1);
  svgPic[3].position.set(-76, 13, 0);
  svgPic[3].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(150, 30), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  svgPic[3].plane.position.set(67, 10, 0);
  svgPic[3].add(svgPic[3].plane);
  svgPic[3].box.position.set(-115, -1780, 1);
  svgPic[3].box.scale.set(0.6, 0.6, 1);
  svgPic[3].material.opacity = 1;
  legalText[1] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`© ООО «Умный ритейл», 2022`, 8), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  legalText[1].position.set(0, -1784, 0);
  legalText[2] = new THREE.Object3D();
  legalText[2].line = [];
  for (let i = 0; i < 2; i++) {
    legalText[2].line[i] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(someText[7][i], 9), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
    legalText[2].line[i].position.set(-350, -1530 - 16 * i, 0);
    page[1].add(legalText[2].line[i]);
  }
  createSVG(5, 12, 0xffffff, 0);
  svgPic[5].scale.set(1.1, -1.1, 1);
  svgPic[5].position.set(-76, 13, 0);
  svgPic[5].plane = new THREE.Mesh(new THREE.PlaneBufferGeometry(150, 30), new THREE.MeshBasicMaterial({ color: 0x000000, transparent: true, opacity: 0.0000001 }));
  svgPic[5].plane.position.set(67, 10, 0);
  svgPic[5].add(svgPic[5].plane);
  svgPic[5].box.position.set(-305, -1570, 1);
  svgPic[5].box.scale.set(0.6, 0.6, 1);
  svgPic[5].material.opacity = 1;
  legalText[3] = new THREE.Mesh(new THREE.ShapeBufferGeometry(EuclidCircularARegular.generateShapes(`© ООО «Умный ритейл», 2022`, 10), 2), new THREE.MeshBasicMaterial({ color: 0xffffff, transparent: true, opacity: 1 }));
  legalText[3].position.set(146, -1572, 0);
  pic[13].opacity = 1;
  slideIcon.plane = new THREE.Mesh(new THREE.CircleBufferGeometry(22.5, 64), whiteMaterial);
  slideIcon.icon = new THREE.Mesh(new THREE.PlaneBufferGeometry(pic[13].map.image.width * 0.33, pic[13].map.image.height * 0.33), pic[13]);
  slideIcon.icon.position.set(0, 3, 1);
  gsap.to(slideIcon.icon.position, { duration: 0.7, y: -2, ease: "power1.inOut", repeat: -1, yoyo: true });
  slideIcon.add(slideIcon.plane, slideIcon.icon);
  slideIcon.scale.set(0, 0, 1);
  slideIcon.position.set(0, -365, 2);
  page[0].add(sparkle[15], sparkle[16], sparkle[14], sparkle[13], sparkle[12], svgPic[4].box, slideIcon, legalText[1], svgPic[3].box, sparkle[0], sparkle[1], sparkle[2], sparkle[3], appButton[0], footerText[0], sandwich[0], diceContainer[0], svgPic[0].box, stuffName[0], introText[0], page[0].illustration[0], page[0].illustration[1], page[0].illustration[2], page[0].illustration[3], page[0].illustration[4], page[0].illustration[5]);
  page[0].position.z = -350;
  page[1].add(legalText[3], svgPic[5].box, appButton[1], footerText[1], sparkle[4], sparkle[5], sparkle[6], sparkle[7], sparkle[8], sparkle[9], sparkle[10], sparkle[11], sandwich[1], page[1].illustration[0], page[1].illustration[1], page[1].illustration[2], page[1].illustration[3], page[1].illustration[4], page[1].illustration[5], svgPic[2].box, introText[1], diceContainer[1], eyes[0], svgPic[1].box, stuffName[1]);
  page[1].position.z = -350;
  mainContainer.add(page[0], page[1], backPic);
  pic[0].opacity = 1;
  pic[1].opacity = 1;
  setTimeout(function() {
    goPermanentAnimations();
    startThisStuff();
  }, 200);
}
function startThisStuff() {
  gsap.to([svgPic[0].box.scale, svgPic[1].box.scale], { duration: 1, x: 1, y: 1, ease: "back.out" });
  gsap.to([svgPic[0].material, svgPic[1].material], { duration: 1, opacity: 1, ease: "power1.out" });
  gsap.to([svgPic[0].material.color, svgPic[1].material.color], { duration: 1, r: redColor.r, g: redColor.g, b: redColor.b, ease: "power1.inOut" });
  gsap.to(blackMaterial, { duration: 1, opacity: 1, ease: "power1.inOut" });
  gsap.to([stuffName[0].line[0].material, stuffName[1].material], { duration: 0.4, opacity: 1, ease: "power1.inOut", delay: 0.2 });
  gsap.from(stuffName[0].line[0].rotation, { duration: 0.4, z: -0.2, ease: "power1.inOut", delay: 0.2 });
  gsap.from([stuffName[0].line[0].scale, stuffName[1].scale], { duration: 0.4, y: 0, ease: "power1.inOut", delay: 0.2 });
  gsap.to(stuffName[0].line[1].material, { duration: 0.4, opacity: 1, ease: "power1.inOut", delay: 0.3 });
  gsap.from(stuffName[0].line[1].rotation, { duration: 0.4, z: -0.2, ease: "power1.inOut", delay: 0.3 });
  gsap.from(stuffName[0].line[1].scale, { duration: 0.4, y: 0, ease: "power1.inOut", delay: 0.3 });
  gsap.to(stuffName[0].position, { duration: 0.9, y: -135, ease: "power1.out" });
  gsap.to(stuffName[1].position, { duration: 0.9, y: -220, ease: "power1.out" });
  gsap.to([eyes[0].scale, slideIcon.scale], { duration: 0.5, x: 1, y: 1, ease: "back.out", delay: 0.9 });
  gsap.from(eyes[0].position, { duration: 0.5, x: -70, ease: "back.out", delay: 0.9 });
  for (let i = 0; i < 5; i++) {
    gsap.to(introText[0].line[i].material, { duration: 0.4, opacity: 1, ease: "power1.inOut", delay: 0.6 + 0.1 * i });
    gsap.from(introText[0].line[i].rotation, { duration: 0.4, z: -0.2, ease: "power1.inOut", delay: 0.6 + 0.1 * i });
    gsap.from(introText[0].line[i].scale, { duration: 0.4, y: 0, ease: "power1.inOut", delay: 0.6 + 0.1 * i });
  }
  gsap.to(introText[0].position, { duration: 0.9, y: -224, ease: "power1.out", delay: 0.6 });
  for (let i = 0; i < 3; i++) {
    gsap.to([introText[1].line[i].material, diceContainer[1].text[i].material], { duration: 0.4, opacity: 1, ease: "power1.inOut", delay: 0.6 + 0.1 * i });
    gsap.from(introText[1].line[i].scale, { duration: 0.4, y: 0, ease: "power1.inOut", delay: 0.6 + 0.1 * i });
  }
  gsap.to(diceContainer[1].dice.scale, { duration: 0.6, x: 0.8, y: 0.8, z: 0.8, ease: "back.out" });
  gsap.to(introText[1].position, { duration: 0.9, y: -340, ease: "power1.out", delay: 0.6 });
  gsap.from(page[1].illustration[0].position, { duration: 1, x: -800, ease: "power2.out", delay: 0.3 });
  gsap.from(page[0].illustration[0].position, { duration: 1, x: -200, ease: "power2.out", delay: 0.3 });
  gsap.to(pic[2], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.3 });
  gsap.from(page[1].illustration[1].position, { duration: 1, x: 800, ease: "power2.out", delay: 0.3 });
  gsap.from([page[0].illustration[1].position, page[0].illustration[3].position], { duration: 1, x: 200, ease: "power2.out", delay: 0.3 });
  gsap.to(pic[3], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.3 });
  gsap.to(pic[5], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.4 });
  gsap.from(page[1].illustration[4].position, { duration: 1, y: -1200, ease: "power2.out", delay: 0.5 });
  gsap.from(page[0].illustration[4].position, { duration: 1, x: 200, ease: "power2.out", delay: 0.5 });
  gsap.to(pic[6], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.5 });
  gsap.from(page[1].illustration[2].position, { duration: 1, x: 800, ease: "power2.out", delay: 0.6 });
  gsap.from(page[0].illustration[2].position, { duration: 1, x: -200, ease: "power2.out", delay: 0.6 });
  gsap.to(pic[4], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.6 });
  gsap.from(page[1].illustration[5].position, { duration: 1, x: -800, ease: "power2.out", delay: 0.6 });
  gsap.from(page[0].illustration[5].position, { duration: 1, x: 200, ease: "power2.out", delay: 0.7 });
  gsap.to(pic[7], { duration: 1, opacity: 1, ease: "power1.inOut", delay: 0.7 });
  gsap.to([svgPic[2].material, svgPic[4].material], { duration: 3, opacity: 1, ease: "power2.out" });
  gsap.to([pic[15], pic[16], pic[19], pic[20]], { duration: 4, opacity: 1, ease: "power1.inOut" });
  gsap.to(svgPic[4].box.position, { duration: 4, y: -460, ease: "power2.out" });
  gsap.to(svgPic[2].box.position, { duration: 4, y: -525, ease: "power2.out" });
  gsap.to(backPic.fog.material, { duration: 2, opacity: 0, ease: "power1.out" });
  document.body.addEventListener('touchend', onDocumentTouchEnd, false);
  document.body.addEventListener('wheel', onWheel, false);
  document.body.addEventListener('touchstart', onDocumentTouchStart, false);
  document.body.addEventListener('mousedown', onDocumentMouseDown, false);
  document.body.addEventListener('mousemove', onDocumentMouseMove, false);
}
function goPermanentAnimations() {
  gsap.to(eyes[0].part[1].position, { duration: 0.2 + Math.random() * 0.3, x: -6 + Math.random() * 12, ease: "power1.inOut", delay: Math.random() * 0.6, onComplete: function() {
    goPermanentAnimations();
  } });
}
function drawCorner(shape, posX, posY, radius, size, curve, clock) {
  if (clock == 0) {
    shape.bezierCurveTo(posX, posY - radius, posX, posY - radius + curve, posX + radius - size, posY - radius + size);
    shape.bezierCurveTo(posX + radius - size, posY - radius + size, posX + radius - curve, posY, posX + radius, posY);
  } else if (clock == 1) {
    shape.bezierCurveTo(posX - radius, posY, posX - radius + curve, posY, posX - radius + size, posY - radius + size);
    shape.bezierCurveTo(posX - radius + size, posY - radius + size, posX, posY - radius + curve, posX, posY - radius);
  } else if (clock == 2) {
    shape.bezierCurveTo(posX, posY + radius, posX, posY + radius - curve, posX - radius + size, posY + radius - size);
    shape.bezierCurveTo(posX - radius + size, posY + radius - size, posX - radius + curve, posY, posX - radius, posY);
  } else {
    shape.bezierCurveTo(posX + radius, posY, posX + radius - curve, posY, posX + radius - size, posY + radius - size);
    shape.bezierCurveTo(posX + radius - size, posY + radius - size, posX, posY + radius - curve, posX, posY + radius);
  }
}
function createSVG(chosen, details, color, targetPath) {
  svgPic[chosen] = new THREE.Object3D();
  svgPic[chosen].material = new THREE.MeshBasicMaterial({ color: color, transparent: true, opacity: 0 });
  for (let i = 0; i < path[targetPath].length; i++) {
    const newPath = path[targetPath][i];
    const shapes = newPath.toShapes(true);
    for (let j = 0; j < shapes.length; j++) {
      const mesh = new THREE.Mesh(new THREE.ShapeBufferGeometry(shapes[j], details), svgPic[chosen].material);
      svgPic[chosen].add(mesh);
      svgPic[chosen].box = new THREE.Object3D();
      svgPic[chosen].box.add(svgPic[chosen]);
    }
  }
}
let onSlide = false;
let oldDeltaY = 0;
let oldScaleY = 0;
let hammertime = new Hammer(document.body);
hammertime.get('pan').set({ enable: true });
hammertime.get('pan').set({ direction: Hammer.DIRECTION_ALL });
hammertime.on('pan', function(event) {
  if (page[0].visible) {
    if (!onSlide) {
      page[0].position.y += oldDeltaY - event.deltaY / mainContainer.scale.x;
      if (page[0].position.y < portraitContainerOffsetY) {
        page[0].scale.y -= (oldScaleY - event.deltaY / mainContainer.scale.x) / 10000;
        page[0].position.y = portraitContainerOffsetY;
        oldScaleY = event.deltaY / mainContainer.scale.x;
      } 
      if (page[0].position.y > -portraitContainerOffsetY + 1820 * page[0].scale.y) {
        page[0].scale.y -= (oldScaleY + event.deltaY / mainContainer.scale.x) / 500000;
        page[0].position.y = -portraitContainerOffsetY + 1820 * page[0].scale.y;
        oldScaleY = event.deltaY / mainContainer.scale.x;
      } 
      if (page[0].position.y > portraitContainerOffsetY && page[0].position.y < -portraitContainerOffsetY + 1820 * page[0].scale.y) {
        if (page[0].scale.y > 1) {
          page[0].scale.y -= (page[0].scale.y - 1) / 10;
          if (page[0].position.y > -portraitContainerOffsetY + 1820 * page[0].scale.y) {
            page[0].position.y = -portraitContainerOffsetY + 1820 * page[0].scale.y;
          }
        }
      }
      oldDeltaY = event.deltaY / mainContainer.scale.x;
        if (event.eventType == 2) {
        if (page[0].slideTween !== undefined && page[0].slideTween !== null) {
          page[0].slideTween.kill();
          page[0].slideTween = null;
        }
      }
      if (event.eventType == 4) {
        oldDeltaY = 0;
        oldScaleY = 0;
        if (page[0].scaleTween !== undefined && page[0].scaleTween !== null) {
          page[0].scaleTween.kill();
          page[0].scaleTween = null;
        }
        page[0].scaleTween = gsap.to(page[0].scale, { duration: 0.2, y: 1, ease: "power1.out" });
        let slideTo = -event.velocityY / mainContainer.scale.x * 300;
        if (slideTo + page[0].position.y < portraitContainerOffsetY) slideTo = portraitContainerOffsetY - page[0].position.y;
        if (slideTo + page[0].position.y > -portraitContainerOffsetY + 1820) slideTo = -portraitContainerOffsetY - page[0].position.y + 1820;
        if (Math.abs(slideTo) < 8) slideTo = 0;
        let slideTime = Math.abs(slideTo / 400);
        if (slideTime < 0.4) slideTime = 0.4;
        if (page[0].slideTween !== undefined && page[0].slideTween !== null) {
          page[0].slideTween.kill();
          page[0].slideTween = null;
        }
        page[0].slideTween = gsap.to(page[0].position, { duration: slideTime, y: page[0].position.y + slideTo, ease: "power2.out", onUpdate: function() {
          if (page[0].position.y > -portraitContainerOffsetY + 1820) {
            page[0].position.y = -portraitContainerOffsetY + 1820 * page[0].scale.y;
          }
        } });
      }
    }
  } else {
    page[1].position.y += oldDeltaY - event.deltaY / mainContainer.scale.x;
    if (page[1].position.y < albumContainerOffsetY) {
      page[1].scale.y -= (oldScaleY - event.deltaY / mainContainer.scale.x) / 10000;
      page[1].position.y = albumContainerOffsetY;
      oldScaleY = event.deltaY / mainContainer.scale.x;
    } 
    if (page[1].position.y > -albumContainerOffsetY + 1645 * page[1].scale.y) {
      page[1].scale.y -= (oldScaleY + event.deltaY / mainContainer.scale.x) / 300000;
      page[1].position.y = -albumContainerOffsetY + 1645 * page[1].scale.y;
      oldScaleY = event.deltaY / mainContainer.scale.x;
    } 
    if (page[1].position.y > albumContainerOffsetY && page[1].position.y < -albumContainerOffsetY + 1645 * page[1].scale.y) {
      if (page[1].scale.y > 1) {
        page[1].scale.y -= (page[1].scale.y - 1) / 10;
        if (page[1].position.y > -albumContainerOffsetY + 1645 * page[1].scale.y) {
          page[1].position.y = -albumContainerOffsetY + 1645 * page[1].scale.y;
        }
      }
    }
    oldDeltaY = event.deltaY / mainContainer.scale.x;
    if (event.eventType == 2) {
      if (page[1].slideTween !== undefined && page[1].slideTween !== null) {
        page[1].slideTween.kill();
        page[1].slideTween = null;
      }
    }
    if (event.eventType == 4) {
      oldDeltaY = 0;
      oldScaleY = 0;
      if (page[1].scaleTween !== undefined && page[1].scaleTween !== null) {
        page[1].scaleTween.kill();
        page[1].scaleTween = null;
      }
      page[1].scaleTween = gsap.to(page[1].scale, { duration: 0.2, y: 1, ease: "power1.out" });
      let slideTo = -event.velocityY / mainContainer.scale.x * 300;
      if (slideTo + page[1].position.y < albumContainerOffsetY) slideTo = albumContainerOffsetY - page[1].position.y;
      if (slideTo + page[1].position.y > -albumContainerOffsetY + 1645) slideTo = -albumContainerOffsetY - page[1].position.y + 1645;
      if (Math.abs(slideTo) < 8) slideTo = 0;
      let slideTime = Math.abs(slideTo / 400);
      if (slideTime < 0.4) slideTime = 0.4;
      if (page[1].slideTween !== undefined && page[1].slideTween !== null) {
        page[1].slideTween.kill();
        page[1].slideTween = null;
      }
      page[1].slideTween = gsap.to(page[1].position, { duration: slideTime, y: page[1].position.y + slideTo, ease: "power2.out", onUpdate: function() {
        if (page[1].position.y > -albumContainerOffsetY + 1645) {
          page[1].position.y = -albumContainerOffsetY + 1645 * page[1].scale.y;
        }
      } });
    }
  }
});
function rollDice() {
  if (firstRoll) {
    diceContainer[0].dice.tween1.kill();
    diceContainer[0].dice.tween1 = null;
    diceContainer[0].dice.tween2.kill();
    diceContainer[0].dice.tween2 = null;
    diceContainer[0].dice.tween3.kill();
    diceContainer[0].dice.tween3 = null;
    for (let i = 0; i < 4; i++) {
      gsap.to(diceContainer[0].text[i].material, { duration: 0.4, opacity: 0, ease: "power1.in", delay: 0.1 * i });
      gsap.to(diceContainer[0].text[i].position, { duration: 0.4, y: diceContainer[0].text[i].position.x + 10, ease: "power1.in", delay: 0.1 * i });
    }
    for (let i = 0; i < 3; i++) {
      gsap.to(diceContainer[1].text[i].material, { duration: 0.4, opacity: 0, ease: "power1.in", delay: 0.1 * i });
      gsap.to(diceContainer[1].text[i].position, { duration: 0.4, y: diceContainer[1].text[i].position.x + 15, ease: "power1.in", delay: 0.1 * i });
    }
  }
  gsap.to([diceContainer[0].button.scale, diceContainer[1].button.scale], { duration: 0.2, x: 0.8, y: 0.8, z: 0.8, ease: "back.out" });
  gsap.to([diceContainer[0].button.plane.material, diceContainer[0].button.text.material, diceContainer[1].button.plane.material, diceContainer[1].button.text.material], { duration: 0.4, opacity: 0, ease: "power2.out" });
  gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 0.5, x: diceContainer[0].dice.rotation.x - Math.random() * 6, y: diceContainer[0].dice.rotation.y - Math.random() * 6, z: diceContainer[0].dice.rotation.z - Math.random() * 6, ease: "power2.out" });
  gsap.to([diceContainer[0].dice.scale, diceContainer[1].dice.scale], { duration: 0.5, x: 0.5, y: 0.5, z: 0.5, ease: "power2.in", onComplete: function() {
    gsap.to(diceContainer[0].dice.position, { duration: 1.5, y: -225, ease: "power1.out" });
    gsap.to(diceContainer[1].dice.position, { duration: 1.5, y: -221, ease: "power1.out" });
    gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 2 + Math.random(), x: Math.floor(diceContainer[0].dice.rotation.x / (Math.PI / 2) + 4 + Math.floor(Math.random() * 4)) * Math.PI * 0.5, ease: "power2.out", });
    gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 2 + Math.random(), y: Math.floor(diceContainer[0].dice.rotation.y / (Math.PI / 2) + 4 + Math.floor(Math.random() * 4)) * Math.PI * 0.5, ease: "power2.out", });
    gsap.to([diceContainer[0].dice.rotation, diceContainer[1].dice.rotation], { duration: 2 + Math.random(), z: Math.floor(diceContainer[0].dice.rotation.z / (Math.PI / 2) + 4 + Math.floor(Math.random() * 4)) * Math.PI * 0.5, ease: "power2.out", });
    gsap.to(diceContainer[1].dice.scale, { duration: 1.5, x: 1.5, y: 1.5, z: 1.5, ease: "power2.out" });
    gsap.to(diceContainer[0].dice.scale, { duration: 1.5, x: 1.15, y: 1.15, z: 1.15, ease: "power2.out", onComplete: function() {
      gsap.to(diceContainer[0].dice.position, { duration: 0.5, y: -200, ease: "power4.in" });
      gsap.to(diceContainer[1].dice.position, { duration: 0.5, y: -190, ease: "power4.in" });
      gsap.to(diceContainer[1].dice.scale, { duration: 0.5, x: 1.3, y: 1.3, z: 1.3, ease: "power4.in" });
      gsap.to(diceContainer[0].dice.scale, { duration: 0.5, x: 1, y: 1, z: 1, ease: "power4.in", onComplete: function() {
        for (let i = 0; i < 12; i++) {
          whiteSparkle[i].position.set(0, -200, 1);
          whiteSparkle[i].scale.set(3, 3, 1);
          gsap.to(whiteSparkle[i].position, { duration: 0.4 + Math.random() * 0.2, x: -180 + Math.random() * 360, y: -380 + Math.random() * 360, z: 1, ease: "power3.out" });
          gsap.to(whiteSparkle[i].scale, { duration: 0.3 + Math.random() * 0.3, x: 0, y: 0, ease: "bounce.out" });
        }
        for (let i = 12; i < 24; i++) {
          whiteSparkle[i].position.set(0, -200, 1);
          whiteSparkle[i].scale.set(3.5, 3.5, 1);
          gsap.to(whiteSparkle[i].position, { duration: 0.4 + Math.random() * 0.2, x: -250 + Math.random() * 500, y: -380 + Math.random() * 380, z: 1, ease: "power3.out" });
          gsap.to(whiteSparkle[i].scale, { duration: 0.3 + Math.random() * 0.3, x: 0, y: 0, ease: "bounce.out" });
        }
        if (firstRoll) {
          firstRoll = false;
          diceContainer[0].button.text.geometry.dispose();
          diceContainer[0].button.text.geometry = new THREE.ShapeBufferGeometry(EuclidCircularABold.generateShapes(`Кинь ещё!`, 14), 4);
          diceContainer[0].button.text.geometry.center();
          diceContainer[0].button.text.geometry.NeedUpdate = true;
          diceContainer[1].button.text.geometry.dispose();
          diceContainer[1].button.text.geometry = new THREE.ShapeBufferGeometry(EuclidCircularABold.generateShapes(`Кинь ещё!`, 14), 4);
          diceContainer[1].button.text.geometry.center();
          diceContainer[1].button.text.geometry.NeedUpdate = true;
        }
        gsap.to(diceContainer[0].dice.scale, { duration: 0.07, x: 1.05, y: 1.05, z: 1.05, ease: "power1.inOut", repeat: 3, yoyo: true });
        gsap.to(diceContainer[1].dice.scale, { duration: 0.07, x: 1.35, y: 1.35, z: 1.35, ease: "power1.inOut", repeat: 3, yoyo: true });
        gsap.to([diceContainer[0].button.plane.material, diceContainer[0].button.text.material, diceContainer[1].button.plane.material, diceContainer[1].button.text.material], { duration: 0.3, opacity: 1, ease: "power1.in", delay: 0.2 });
        gsap.to([diceContainer[0].button.scale, diceContainer[1].button.scale], { duration: 0.3, x: 1, y: 1, z: 1, ease: "back.out", delay: 0.2, onComplete: function() {
          diceContainer[0].button.ready = true;
          diceContainer[1].button.ready = true;
        } });
      } });
    } });
  } });
}
let touchReady = true;
const raycaster = new THREE.Raycaster(), mouse = new THREE.Vector2();
function onDocumentTouchStart(event) {
  event.preventDefault();
  if (touchReady) {
    touchReady = false;
    if (page[0].visible) {
      if (page[0].slideTween !== undefined && page[0].slideTween !== null) {
        page[0].slideTween.kill();
        page[0].slideTween = null;
      }
    }
    if (page[1].visible) {
      if (page[1].slideTween !== undefined && page[1].slideTween !== null) {
        page[1].slideTween.kill();
        page[1].slideTween = null;
      }
    }
    mouse.x = (event.touches[0].clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.touches[0].clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, mainCamera);
    checkInteractive(raycaster);
  }
}
function onDocumentMouseDown(event) {
  event.preventDefault();
  if (touchReady) {
    touchReady = false;
    mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
    mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
    raycaster.setFromCamera(mouse, mainCamera);
    checkInteractive(raycaster);
  }
}
function onWheel(event) {
  event.preventDefault();
  if (page[0].visible) {
    page[0].position.y += event.deltaY * 0.2;
    if (page[0].position.y < portraitContainerOffsetY) {
      page[0].position.y = portraitContainerOffsetY;
    }
    if (page[0].position.y > -portraitContainerOffsetY + 1820) {
      page[0].position.y = -portraitContainerOffsetY + 1820;
    }
  }
  if (page[1].visible) {
    page[1].position.y += event.deltaY * 0.2;
    if (page[1].position.y < albumContainerOffsetY) {
      page[1].position.y = albumContainerOffsetY;
    }
    if (page[1].position.y > -albumContainerOffsetY + 1645) {
      page[1].position.y = -albumContainerOffsetY + 1645;
    }
  }
}
function onDocumentTouchEnd(event) {
  event.preventDefault();
  raycaster.setFromCamera(mouse, mainCamera);
  intersects = raycaster.intersectObject(svgPic[0].plane);
  if (intersects.length > 0) {
    window.open("https://samokat.ru/");
  }
  intersects = raycaster.intersectObject(svgPic[3].plane);
  if (intersects.length > 0) {
    window.open("https://samokat.ru/");
  }
  intersects = raycaster.intersectObject(svgPic[5].plane);
  if (intersects.length > 0) {
    window.open("https://samokat.ru/");
  }
  intersects = raycaster.intersectObject(appButton[0]);
  if (intersects.length > 0) {
    window.open("https://vml8.adj.st/category/6e226e85-28e3-4ee2-8781-95e9e04ccaf5?showcaseType=MINIMARKET&adj_t=8fekc10");
  }
  intersects = raycaster.intersectObject(appButton[1]);
  if (intersects.length > 0) {
    window.open("https://vml8.adj.st/category/6e226e85-28e3-4ee2-8781-95e9e04ccaf5?showcaseType=MINIMARKET&adj_t=8fekc10");
  }
}
function checkInteractive(raycaster) {
  intersects = raycaster.intersectObject(diceContainer[0].button.plane);
  if (intersects.length > 0 && diceContainer[0].button.ready && page[0].visible) {
    diceContainer[0].button.ready = false;
    diceContainer[1].button.ready = false;
    if (firstRoll) {
      gtag('event', '<drop>');
    } else {
      gtag('event', '<drop_again>');
    }
    rollDice();
  }
  intersects = raycaster.intersectObject(diceContainer[1].button.plane);
  if (intersects.length > 0 && diceContainer[1].button.ready && page[1].visible) {
    diceContainer[0].button.ready = false;
    diceContainer[1].button.ready = false;
    if (firstRoll) {
      gtag('event', '<drop>');
    } else {
      gtag('event', '<drop_again>');
    }
    rollDice();
  }
  intersects = raycaster.intersectObject(slideIcon.plane);
  if (intersects.length > 0 && !onSlide && page[0].visible) {
    onSlide = true;
    gsap.to(slideIcon.scale, { duration: 0.3, x: 0.4, y: 0.4, ease: "power3.out", repeat: 1, yoyo: true });
    gsap.to(page[0].position, { duration: 1, y: 1005, ease: "power1.inOut", onComplete: function() {
      onSlide = false;
    } });
  }
  if (!isMobile()) {
    intersects = raycaster.intersectObject(svgPic[0].plane);
    if (intersects.length > 0) {
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(svgPic[3].plane);
    if (intersects.length > 0 && page[0].visible) {
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(svgPic[5].plane);
    if (intersects.length > 0) {
      window.open("https://samokat.ru/");
    }
    intersects = raycaster.intersectObject(appButton[0]);
    if (intersects.length > 0) {
      window.open("https://vml8.adj.st/category/6e226e85-28e3-4ee2-8781-95e9e04ccaf5?showcaseType=MINIMARKET&adj_t=8fekc10");
    }
    intersects = raycaster.intersectObject(appButton[1]);
    if (intersects.length > 0) {
      window.open("https://vml8.adj.st/category/6e226e85-28e3-4ee2-8781-95e9e04ccaf5?showcaseType=MINIMARKET&adj_t=8fekc10");
    }
  }
  setTimeout(function() {
    touchReady = true;
  }, 100);
}
function onDocumentMouseMove(event) {
  event.preventDefault();
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
  raycaster.setFromCamera(mouse, mainCamera);
  document.body.style.cursor = "default";
  intersects = raycaster.intersectObject(svgPic[0].plane);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(svgPic[3].plane);
  if (intersects.length > 0 && page[0].visible) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(svgPic[5].plane);
  if (intersects.length > 0) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(appButton[0]);
  if (intersects.length > 0 && page[0].visible) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(appButton[1]);
  if (intersects.length > 0 && page[1].visible) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(slideIcon);
  if (intersects.length > 0 && !onSlide && page[0].visible) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(diceContainer[0].button.plane);
  if (intersects.length > 0 && diceContainer[0].button.ready && page[0].visible) {
    document.body.style.cursor = "pointer";
  }
  intersects = raycaster.intersectObject(diceContainer[1].button.plane);
  if (intersects.length > 0 && diceContainer[1].button.ready && page[1].visible) {
    document.body.style.cursor = "pointer";
  }
}
let oldWindow = document.body.clientWidth / document.body.clientHeight;
onWindowResize();
function onWindowResize() {
  if (document.documentElement.clientHeight / document.documentElement.clientWidth > 1) {
    page[0].visible = true;
    page[1].visible = false;
    portraitContainerOffsetY = (document.documentElement.clientHeight / document.documentElement.clientWidth) * 180;
    if (page[0].position.y < portraitContainerOffsetY) {
      page[0].position.y = portraitContainerOffsetY;
    }
    mainContainer.scale.set(document.documentElement.clientWidth / 360, document.documentElement.clientWidth / 360, 1);
    backPic.scale.set(document.documentElement.clientWidth / 900, document.documentElement.clientWidth / 900, 1);
  } else {
    page[1].visible = true;
    page[0].visible = false;
    albumContainerOffsetY = (document.documentElement.clientHeight / document.documentElement.clientWidth) * 720 + 50;
    if (page[1].position.y < albumContainerOffsetY) {
      page[1].position.y = albumContainerOffsetY;
    }
    mainContainer.scale.set(document.documentElement.clientWidth / 1440, document.documentElement.clientWidth / 1440, 1);
    backPic.scale.set(0.67, 0.67, 1);
  }
  mainRenderer.setPixelRatio(window.devicePixelRatio)
  mainRenderer.setSize(document.body.clientWidth, document.body.clientHeight);
  mainCamera.left = -document.body.clientWidth / 2;
  mainCamera.right = document.body.clientWidth / 2;
  mainCamera.top = document.body.clientHeight / 2;
  mainCamera.bottom = -document.body.clientHeight / 2;
  mainCamera.updateProjectionMatrix();
  oldWindow = document.body.clientWidth / document.body.clientHeight;
}
loop();
function loop() {
  if (oldWindow !== document.body.clientWidth / document.body.clientHeight) {
    onWindowResize();
  }
  mainRenderer.render(mainScene, mainCamera);
  requestAnimationFrame(loop);
}
